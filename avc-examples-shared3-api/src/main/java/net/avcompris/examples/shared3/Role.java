package net.avcompris.examples.shared3;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.examples.shared3.Permission.ANY;
import static net.avcompris.examples.shared3.Permission.CREATE_ANY_USER;
import static net.avcompris.examples.shared3.Permission.DELETE_ANY_USER;
import static net.avcompris.examples.shared3.Permission.GET_ANY_USER;
import static net.avcompris.examples.shared3.Permission.GET_ANY_USER_SESSION;
import static net.avcompris.examples.shared3.Permission.GET_MY_SESSION;
import static net.avcompris.examples.shared3.Permission.GET_USER_ME;
import static net.avcompris.examples.shared3.Permission.PURGE_CORRELATION_IDS;
import static net.avcompris.examples.shared3.Permission.QUERY_ALL_USERS;
import static net.avcompris.examples.shared3.Permission.ROUTE;
import static net.avcompris.examples.shared3.Permission.SET_LAST_ACTIVE_AT;
import static net.avcompris.examples.shared3.Permission.TERMINATE_ANY_USER_SESSION;
import static net.avcompris.examples.shared3.Permission.TERMINATE_MY_SESSION;
import static net.avcompris.examples.shared3.Permission.UPDATE_ANY_USER;
import static net.avcompris.examples.shared3.Permission.UPDATE_USER_ME;
import static net.avcompris.examples.shared3.Permission.WORKERS;

import javax.annotation.Nullable;

import org.apache.commons.lang3.ArrayUtils;

import net.avcompris.commons3.api.EnumRole;

public enum Role implements EnumRole {

	ANONYMOUS(null,

			ANY //
	// GET_APP_INFO //
	),

	REGULAR(new Role[] { ANONYMOUS },

			SET_LAST_ACTIVE_AT, GET_MY_SESSION, TERMINATE_MY_SESSION, //
			GET_USER_ME, UPDATE_USER_ME //
	),

	USERMGR(new Role[] { REGULAR },

			QUERY_ALL_USERS, //
			CREATE_ANY_USER, GET_ANY_USER, UPDATE_ANY_USER, DELETE_ANY_USER, //
			GET_ANY_USER_SESSION, //
			TERMINATE_ANY_USER_SESSION //
	),

	SILENT_WORKER(new Role[] { ANONYMOUS },

			ROUTE),

	ADMIN(new Role[] { USERMGR, SILENT_WORKER },

			PURGE_CORRELATION_IDS, WORKERS),

	SUPERADMIN(new Role[] { ADMIN },

			Permission.SUPERADMIN);

	private final Role[] superRoles;
	private final Permission[] permissions;

	Role(@Nullable final Role[] superRoles, final Permission... permissions) {

		this.superRoles = (superRoles != null) ? superRoles : new Role[0];
		this.permissions = checkNotNull(permissions, "permissions");
	}

	public String getRolename() {

		return name();
	}

	@Override
	public Role[] getSuperRoles() {

		return ArrayUtils.clone(superRoles);
	}

	@Override
	public Permission[] getPermissions() {

		return ArrayUtils.clone(permissions);
	}

	public boolean canManage(final Role role) {

		checkNotNull(role, "role");

		switch (this) {
		case SUPERADMIN:
			return true;
		case ADMIN:
			switch (role) {
			case ADMIN:
			case SILENT_WORKER:
				return true;
			default:
				break;
			}
		case USERMGR:
			switch (role) {
			case USERMGR:
				return true;
			default:
				break;
			}
		default:
			return false;
		}
	}
}
