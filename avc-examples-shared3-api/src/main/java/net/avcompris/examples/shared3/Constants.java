package net.avcompris.examples.shared3;

public interface Constants {

	enum ReservedUsername {

		ME("me"),

		SUPERADMIN("superadmin");

		private final String label;

		ReservedUsername(final String label) {

			this.label = label;
		}

		public String label() {

			return label;
		}
	}
}
