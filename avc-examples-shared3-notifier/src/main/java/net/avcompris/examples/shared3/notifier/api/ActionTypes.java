package net.avcompris.examples.shared3.notifier.api;

import net.avcompris.commons3.notifier.api.Notification.ActionType;

public enum ActionTypes implements ActionType {

	USER_CREATED, USER_UPDATED, USER_DELETED,
}
