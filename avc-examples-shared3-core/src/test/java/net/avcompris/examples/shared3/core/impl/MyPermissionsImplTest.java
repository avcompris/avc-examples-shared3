package net.avcompris.examples.shared3.core.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Service;

import net.avcompris.commons3.api.EnumRole;
import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnauthorizedException;
import net.avcompris.commons3.core.impl.PermissionsImpl;
import net.avcompris.examples.shared3.Permission;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.shared3.core.api.Permissions;

public class MyPermissionsImplTest {

	private interface MyService {

		@Permissions(Permission.CREATE_ANY_USER)
		void call(User user) throws ServiceException;

		@Permissions(Permission.GET_MY_SESSION)
		void callMy(User user) throws ServiceException;
	}

	@Service
	private class MyServiceImpl implements MyService {

		@Override
		public void call(final User user) throws ServiceException {

			new PermissionsImpl().assertAuthorized("XXX", user);
		}

		@Override
		public void callMy(final User user) throws ServiceException {

			new PermissionsImpl().assertAuthorized("XXX", user);
		}
	}

	@Test
	public void test_REGULAR_has_not_CREATE_ANY_USER() throws Exception {

		assertThrows(UnauthorizedException.class, ()

		-> new MyServiceImpl().call(new User() {

			@Override
			public String getUsername() {
				return "yyy";
			}

			@Override
			public EnumRole getRole() {
				return Role.REGULAR;
			}
		}));
	}

	@Test
	public void test_REGULAR_has_GET_MY_SESSION() throws Exception {

		new MyServiceImpl().callMy(new User() {

			@Override
			public String getUsername() {
				return "yyy";
			}

			@Override
			public EnumRole getRole() {
				return Role.REGULAR;
			}
		});
	}
}
