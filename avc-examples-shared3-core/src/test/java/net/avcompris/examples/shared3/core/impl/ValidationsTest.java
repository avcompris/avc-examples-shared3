package net.avcompris.examples.shared3.core.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import net.avcompris.commons3.api.exception.InvalidUsernameException;

public class ValidationsTest {

	@Test
	public void test_username_tooLong() throws Exception {

		assertThrows(InvalidUsernameException.class, ()

		-> Validations.assertValidUsername("blahblahblahblahblahblahblahblah" //
				+ "blahblahblahblahblahblahblahblah" //
				+ "blahblahblahblahblahblahblahblah" //
				+ "blahblahblahblahblahblahblahblah"));
	}

	@Test
	public void test_username_nonAscii() throws Exception {

		assertThrows(InvalidUsernameException.class, ()

		-> Validations.assertValidUsername("très bien"));
	}

	@Test
	public void test_username_illegalCharacter() throws Exception {

		assertThrows(InvalidUsernameException.class, ()

		-> Validations.assertValidUsername("TR*401"));
	}

	@Test
	public void test_username_good() throws Exception {

		Validations.assertValidUsername("TR-401");
	}
}
