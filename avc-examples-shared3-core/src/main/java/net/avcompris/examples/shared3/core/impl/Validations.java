package net.avcompris.examples.shared3.core.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.regex.Pattern;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.exception.InvalidPasswordException;
import net.avcompris.commons3.api.exception.InvalidRoleException;
import net.avcompris.commons3.api.exception.InvalidUsernameException;
import net.avcompris.commons3.api.exception.ReservedUsernameException;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.shared3.Constants.ReservedUsername;

public abstract class Validations {

	private static final Pattern USERNAME_PATTERN = Pattern.compile("^[a-zA-Z][a-zA-Z0-9-_]*$");

	public static void assertValidUsername(final String username)
			throws InvalidUsernameException, ReservedUsernameException {

		checkNotNull(username, "username");

		if (ReservedUsername.ME.label().contentEquals(username)) {

			throw new ReservedUsernameException(username);
		}

		try {

			assertNotBlank("Username", username);
			assertMaxLength("Username", 40, username);
			assertASCII("Username", username);
			assertRegexp("Username", USERNAME_PATTERN, username);

		} catch (final ValidationException e) {

			throw new InvalidUsernameException(e.getMessage());
		}
	}

	public static void assertNonSuperadminUsername(@Nullable final String username) throws ReservedUsernameException {

		if (ReservedUsername.SUPERADMIN.label().contentEquals(username)) {

			throw new ReservedUsernameException(username);
		}
	}

	public static void assertValidPassword(final String password) throws InvalidPasswordException {

		checkNotNull(password, "password");

		try {

			assertNotBlank("Password", password);
			assertMaxLength("Password", 40, password);
			assertASCII("Password", password);
			// assertRegexp("Password", PASSWORD_PATTERN, password);

		} catch (final ValidationException e) {

			throw new InvalidPasswordException(e.getMessage());
		}
	}

	public static void assertValidRole(final Role role) throws InvalidRoleException {

		// do nothing
	}

	private static class ValidationException extends Exception {

		private static final long serialVersionUID = -8064040017766344235L;

		ValidationException(final String message) {

			super(message);
		}
	}

	private static void assertNotBlank(final String label, @Nullable final String s) throws ValidationException {

		if (isBlank(s)) {

			throw new ValidationException(label + " must at least contain some characters.");
		}
	}

	private static void assertMaxLength(final String label, final int maxLength, final String s)
			throws ValidationException {

		if (s.length() > maxLength) {

			throw new ValidationException(
					label + " should contain at most " + maxLength + " characters, but was: " + s.length());
		}
	}

	private static void assertASCII(final String label, final String s) throws ValidationException {

		for (final char c : s.toCharArray()) {

			if (c <= 32 || c > 126) {
				throw new ValidationException(
						label + " must only contain ASCII non space characters: (" + ((int) c) + "): " + c);
			}
		}
	}

	private static void assertRegexp(final String label, final Pattern pattern, final String s)
			throws ValidationException {

		if (!pattern.matcher(s).matches()) {

			throw new ValidationException(label + " should match the regular expression: " + pattern.pattern());
		}
	}
}
