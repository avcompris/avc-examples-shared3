package net.avcompris.examples.shared3.core.api;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import net.avcompris.examples.shared3.Permission;

@Retention(RUNTIME)
@Target(METHOD)
public @interface Permissions {

	Permission[] value();
}
