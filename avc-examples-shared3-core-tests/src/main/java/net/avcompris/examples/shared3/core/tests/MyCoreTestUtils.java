package net.avcompris.examples.shared3.core.tests;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.core.tests.CoreTestUtils;
import net.avcompris.examples.shared3.Constants;
import net.avcompris.examples.shared3.Role;

public abstract class MyCoreTestUtils extends CoreTestUtils {

	public static User defaultUser() {

		return new User() {

			@Override
			public String getUsername() {
				return Constants.ReservedUsername.SUPERADMIN.label();
			}

			@Override
			public Role getRole() {
				return Role.SUPERADMIN;
			}
		};
	}
}
