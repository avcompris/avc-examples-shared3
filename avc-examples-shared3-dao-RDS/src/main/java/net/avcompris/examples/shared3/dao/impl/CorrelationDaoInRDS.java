package net.avcompris.examples.shared3.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import net.avcompris.commons3.dao.CorrelationDao;
import net.avcompris.commons3.dao.exception.DuplicateEntityException;
import net.avcompris.commons3.dao.impl.AbstractDaoInRDS;
import net.avcompris.commons3.utils.Clock;

@Component
public final class CorrelationDaoInRDS extends AbstractDaoInRDS implements CorrelationDao {

	private final boolean debug;

	@Autowired
	public CorrelationDaoInRDS( //
			@Value("#{rds.dataSource}") final DataSource dataSource, //
			@Value("#{rds.tableNames.correlations}") final String tableName, //
			final Clock clock) {

		super(dataSource, tableName, clock);

		debug = System.getProperty("debug") != null;
	}

	@Override
	public boolean isCorrelationIdValid(final String correlationId) throws SQLException {

		checkNotNull(correlationId, "correlationId");

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " 1" //
					+ " FROM " + tableName //
					+ " WHERE correlation_id = ?" //
			)) {

				setString(pstmt, 1, correlationId);

				try (ResultSet rs = pstmt.executeQuery()) {

					if (rs.next()) {

						return true;
					}
				}
			}
		}

		return false;
	}

	@Override
	public void addCorrelationId(final String correlationId)
			throws SQLException, IOException, DuplicateEntityException {

		checkNotNull(correlationId, "correlationId");

		final DateTime now = clock.now();

		try (Connection cxn = getConnection()) {

			if (debug) {
				final long elapsedMs = System.currentTimeMillis() - now.getMillis();
				System.out.println(CorrelationDaoInRDS.class.getSimpleName() + ".addCorrelationId(): " + correlationId
						+ ": getConnection(), elapsedMs: " + elapsedMs);
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("INSERT INTO " + tableName //
					+ " (correlation_id, created_at)" //
					+ " VALUES (?, ?)" //
			)) {

				setString(pstmt, 1, correlationId);
				setDateTime(pstmt, 2, now);

				try {

					pstmt.executeUpdate();

				} catch (final SQLIntegrityConstraintViolationException e) {

					throw new DuplicateEntityException("Duplicate correlationId: " + correlationId, e);
				}
			}
		}

		if (debug) {
			final long elapsedMs = System.currentTimeMillis() - now.getMillis();
			System.out.println(CorrelationDaoInRDS.class.getSimpleName() + ".addCorrelationId(): " + correlationId
					+ ": total: elapsedMs: " + elapsedMs);
		}
	}

	@Override
	public void purgeOlderThanSec(final int seconds) throws SQLException, IOException {

		final DateTime olderThan = clock.now().minusSeconds(seconds);

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("DELETE FROM " + tableName //
					+ " WHERE created_at < ?" //
			)) {

				setDateTime(pstmt, 1, olderThan);

				pstmt.executeUpdate();
			}
		}
	}
}
